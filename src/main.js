import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import store from './store'
import Home from "./Home";
import Contact from "./Contact";

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/', component: Home },
    { path: '/contact/:id', component: Contact },
  ]
});

new Vue({
  el: '#app',
  router: router,
  store: store,
  render: h => h(App)
})
