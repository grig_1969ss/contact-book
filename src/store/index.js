import Vue from 'vue'
import Vuex from 'vuex'
import contacts from './contacts'

//Load Vuex
Vue.use(Vuex)

//Create store
export default new Vuex.Store({
  modules: {
    contacts
  }
})
