const state = {
  contacts: [
    {id: 1, data: [{name: 'Name', value: 'Anna'}, {name: 'Phone', value: '+37455970348'}]},
    {id: 2, data: [{name: 'Name', value: 'Maria'}, {name: 'Phone', value: '+3749498025'}]},
    {id: 3, data: [{name: 'Name', value: 'Mika'}, {name: 'Phone', value: '+37499990168'}]},
  ]
};

const getters = {
  CONTACTS: state => state.contacts
};

const actions = {};

const mutations = {
  UPDATE_CONTACTS(state, data) {
    state.contacts = JSON.parse(JSON.stringify(data));
  },
  ADD_CONTACTS(state, data) {
    state.contacts = [...state.contacts, data]
  },
};

export default {
  state,
  getters,
  actions,
  mutations
}
